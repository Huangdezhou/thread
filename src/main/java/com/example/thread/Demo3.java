package com.example.thread;

public class Demo3 implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i < 121; i++) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+"我在写代码" + i);
        }
    }

    public static void main(String[] args)throws Exception {
        //创建一个线程
        new Thread(new Demo3()).start();
        //第二个线程
        Thread.sleep(3000);
        new Thread(new Demo3()).start();
        for (int i = 0; i < 111; i++) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("我是主线程" + i);
        }
    }
}
