package com.example.thread;

public class Dem2 implements Runnable {

    public static void main(String[] args) throws Exception {
        Dem2 Demo99 = new Dem2();
        Thread thread = new Thread(Demo99);
        Thread.State state = thread.getState();
        System.out.println(state);
        thread.start();
        state = thread.getState();
        System.out.println(state);
        while (state!=Thread.State.TERMINATED){
            state = thread.getState();
            System.out.println(state);
        }

    }

    @Override
    public void run() {
        for (int i = 0; i < 11; i++) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("end");

    }
}
