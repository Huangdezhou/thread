package com.example.thread;

public class TEST2 {
    public static void main(String[] args) {
        new Thread(new Mythread2()).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 22; i++) {
                    System.out.println(i);
                }
            }
        }).start();
    }
}

class Mythread2 implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            System.out.println(i);
        }
    }
}