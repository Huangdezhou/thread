package com.example.thread;

public class Race implements Runnable {
    private static String winner;

    @Override
    public void run() {
        for (int i = 0; i < 101; i++) {
            if (Thread.currentThread().getName().equals("兔子") && i % 10 == 0){
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
                boolean gameover = gameover(i);
            if (gameover)
                break;
            System.out.println(Thread.currentThread().getName() + "->>" + i);
        }
    }

    boolean gameover(int step) {
        if (winner != null) {
            return true;
        } else {
            if (step >= 100) {
                winner = Thread.currentThread().getName();
                System.out.println(winner + "赢了");
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) throws Exception {
        Race demo5 = new Race();
        new Thread(demo5, "兔子").start();
        new Thread(demo5, "乌龟").start();


    }
}
