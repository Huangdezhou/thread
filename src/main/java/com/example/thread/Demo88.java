package com.example.thread;

public class Demo88 implements Runnable {

    public static void main(String[] args) throws Exception {
        Demo88 demo5 = new Demo88();

 new Thread(demo5,"a").start();
 new Thread(demo5,"b").start();

    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName()+"start");
        Thread.yield();
        System.out.println(Thread.currentThread().getName()+"end");

    }
}
