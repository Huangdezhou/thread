package com.example.thread;

import org.omg.CORBA.INTERNAL;

public class TEST8 {
    static volatile Integer  flag = 0; //volatile 的作用是保证可见性，反正指令重排序

    public static void main(String[] args) throws Exception {
        new Thread(new Runnable() {
            @Override
            public void run() {
                int num = 0;
                while (flag == 0) {
                    num++;
                }
                System.out.println(num);
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(3200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                flag = 2;
            }
        }).start();
    }
}
