package com.example.thread;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class TEST4 {
    public static void main(String[] args) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 22; i++) {
                    System.out.println("t1输出" + i);
                }
            }
        });
        thread.setPriority(10);
        thread.start();
        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 22; i++) {
                    System.out.println("t2输出" + i);
                }
            }
        });

        thread1.setPriority(5);
        thread1.start();
    }
}
