package com.example.thread;

public class TEST13 {

    public static void main(String[] args) {
        Num num = new Num();
        new Thread(new jiThread(num)).start();
        new Thread(new ouThread(num)).start();
    }

    static class ouThread implements Runnable {
        private Num num;

        public ouThread(Num num) {
            this.num = num;
        }

        @Override
        public void run() {
            while (true) {
                if (Num.num > 100) {
                    break;
                } else {
                    synchronized (num) {
                        if (Num.num % 2 == 0) {
                            System.out.println("偶数。。。" + Num.num);
                            Num.num++;

                            num.notify();
                        } else {
                            try {
                                num.wait();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        }
    }

    static class jiThread implements Runnable {
        private Num num;

        public jiThread(Num num) {
            this.num = num;
        }

        @Override
        public void run() {
            while (true) {
                if (Num.num > 100) {
                    break;
                } else {
                    synchronized (num) {
                        if (Num.num % 2 != 0) {
                            System.out.println("奇数。。。" + Num.num);
                            Num.num++;
                            num.notify();
                        } else {
                            try {
                                //
                                num.wait();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        }
    }


    static class Num {
        static int num = 1;
    }
}
