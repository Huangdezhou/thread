package com.example.thread;


import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public class Demo2 implements Runnable {
    private String url;
    private String name;

    public Demo2(String url, String name) {
        this.url = url;
        this.name = name;
    }

    @Override
    public void run() {
        fileDownd.download(url, name);
        System.out.println(name);
    }

    public static void main(String[] args) {
        String url = "https://www.keaidian.com/uploads/allimg/190424/24110307_8.jpg";
        Demo2 d1 = new Demo2(url, "1.jpg");
        Demo2 d2 = new Demo2(url, "2.jpg");
        Demo2 d3 = new Demo2(url, "3.jpg");

        new Thread(d1).start();
        new Thread(d2).start();
        new Thread(d3).start();
    }
}

class fileDownd {
    public static void download(String url, String name) {
        try {
            FileUtils.copyURLToFile(new URL(url), new File(name));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
