package com.example.thread;

public class TEST12 {

    public static void main(String[] args) throws Exception {
        Num num = new Num();
        new Thread(new ouThread(num)).start();
        new Thread(new JiThread(num)).start();

    }

    static class ouThread implements Runnable {
        private Num num;

        public ouThread(Num num) {
            this.num = num;
        }

        @Override
        public void run() {
            while (true) {
                if (Num.num > 100) {
                    break;
                } else {
                    synchronized (num) {
                        if (Num.num % 2 == 0) {
                            System.out.println("偶数==》" + Num.num);
                            Num.num++;
                            try {
                                num.notify();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            try {
                                num.wait();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        }
    }

    static class JiThread implements Runnable {
        Num num;

        public JiThread(Num num) {
            this.num = num;
        }
        @Override
        public void run() {
            while (true) {
                if (Num.num > 100) {
                    break;
                } else {
                    synchronized (num) {
                        if (Num.num % 2 != 0) {
                            System.out.println("奇数==》" + Num.num);
                            Num.num++;
                            try {
                                num.notify();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            try {
                                num.wait();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        }
    }

    static class Num {
        static int num = 1;
    }
}
