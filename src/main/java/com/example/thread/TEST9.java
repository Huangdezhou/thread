package com.example.thread;

public class TEST9 {

    public static void main(String[] args) throws Exception {
        Object o = new Object();
        Object o2 = new Object();
        new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (o) {
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    synchronized (o2) {
                        System.out.println("o2");
                    }
                }
            }
        }).start();
          new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (o2) {
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    synchronized (o ) {
                        System.out.println("o ");
                    }
                }
            }
        }).start();

    }
}
