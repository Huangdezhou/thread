package com.example.thread;

public class Demo5 implements Runnable {
    private Integer ticket = 20;

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (ticket <= 0)
                break;

            System.out.println(Thread.currentThread().getName() + "拿到" + ticket--);
        }
    }

    public static void main(String[] args) throws Exception {
        Demo5 demo5 = new Demo5();
        new Thread(demo5,"小明").start();
        new Thread(demo5,"小红").start();
        new Thread(demo5,"老师").start();


    }
}
