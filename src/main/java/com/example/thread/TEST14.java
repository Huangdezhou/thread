package com.example.thread;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class TEST14 {
    static int carname = 1;

    public static void main(String[] args) {
        Car4s car4s = new Car4s();
        new Thread(new producer(car4s)).start();
        new Thread(new producer(car4s)).start();
        new Thread(new comsumer(car4s)).start();
        new Thread(new comsumer(car4s)).start();
    }

    static class Car {
        public static String getName() {
            return name;
        }

        public static void setName(String name) {
            Car.name = name;
        }

        static String name;
    }

    static class producer implements Runnable {
        Car4s car4s;

        public producer(Car4s car4s) {
            this.car4s = car4s;
        }

        @Override
        public void run() {
            while (true) {
                synchronized (car4s) {
                    if (car4s.carList.size() < 4) {
                        Car car = new Car();
                        car.setName(carname + "");
                        carname++;
                        car4s.carList.push(car);
                        car4s.notifyAll();

                    } else {
                        try {
                            car4s.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    static class comsumer implements Runnable {
        Car4s car4s;

        public comsumer(Car4s car4s) {
            this.car4s = car4s;
        }

        @Override
        public void run() {
            while (true) {
                synchronized (car4s) {
                    if (car4s.carList.size() > 0) {

                        Car poll = (Car) car4s.carList.poll();
                        System.out.println("购买驰车" + (Car) poll);
                        car4s.notifyAll();

                    } else {
                        try {
                            car4s.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    static class Car4s {
        LinkedList carList = new LinkedList();
    }
}
