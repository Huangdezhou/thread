package com.example.thread;


import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.*;

public class Demo66 implements Callable<Boolean> {
    private String url;
    private String name;

    public Demo66(String url, String name) {
        this.url = url;
        this.name = name;
    }


    public static void main(String[] args) {
        String url = "https://www.keaidian.com/uploads/allimg/190424/24110307_8.jpg";

        Demo66 d1 = new Demo66(url, "1.jpg");
        Demo66 d2 = new Demo66(url, "2.jpg");
        Demo66 d3 = new Demo66(url, "3.jpg");

        ExecutorService executorService = Executors.newFixedThreadPool(3);
        Future<Boolean> submit = executorService.submit(d1);
        Future<Boolean> submit1 = executorService.submit(d2);
        Future<Boolean> submit2 = executorService.submit(d3);

        try {
            System.out.println(submit.get() );
            System.out.println(submit1.get());
            System.out.println(submit2.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        executorService.shutdown();

    }

    @Override
    public Boolean call() throws Exception {
        fileDownd2.download(url, name);
        System.out.println(name);
        return true;
    }
}

class fileDownd2 {
    public static void download(String url, String name) {
        try {
            FileUtils.copyURLToFile(new URL(url), new File(name));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
