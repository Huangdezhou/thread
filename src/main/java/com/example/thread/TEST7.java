package com.example.thread;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class TEST7 {
    static Integer num = 100;

    public static void main(String[] args) throws Exception {
        Lock lock = new ReentrantLock();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (lock.tryLock()) {
                        lock.lock();
                        try {
                            if (num > 0) {
                                num--;
                                System.out.println(Thread.currentThread().getName() + "..." + num);
                            }
                        } finally {
                            lock.unlock();
                        }
                    } else {
                        System.out.println("err");
                    }

                }
            }
        };

        Thread thread = new Thread(runnable, "窗口1");
        Thread thread2 = new Thread(runnable, "窗口2");
        Thread thread3 = new Thread(runnable, "窗口3");
        thread.start();
        thread2.start();
        thread3.start();


    }
}
