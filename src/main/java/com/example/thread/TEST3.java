package com.example.thread;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class TEST3 {
    public static void main(String[] args) {
        FutureTask<Integer> integerFutureTask = new FutureTask<>(new Mythread3());
        Thread thread = new Thread(integerFutureTask);
        thread.start();
        try {
            System.out.println(integerFutureTask.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}

class Mythread3 implements Callable<Integer> {

    @Override
    public Integer call() throws Exception {
        Integer in=0;
        for (int i = 0; i < 45; i++) {
            in+=i;
        }
        return in;
    }
}