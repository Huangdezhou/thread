package com.example.thread;

public class Demo1 extends Thread {
    @Override
    public void run() {
        for (int i = 0; i < 121; i++) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("我在写代码" + i);
        }
    }

    public static void main(String[] args) {
        //创建一个线程
        Demo1 demo1 = new Demo1();
//        线程启动
        demo1.start();
        for (int i = 0; i < 111; i++) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("我是主线程" + i);
        }
    }
}
